package com.sensus3d.cpml

import com.sensus3d.cpml.API.CurrencyManager
import com.sensus3d.cpml.API.MercadoLibreRestAPI
import com.sensus3d.cpml.model.Country
import com.sensus3d.cpml.model.CurrencyRatio
import com.sensus3d.cpml.view.MainActivity
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mockito
import retrofit2.Call
import retrofit2.Response
import rx.observers.TestSubscriber

class CurrencyTest {
    private var testSub = TestSubscriber<Float>()
    private var apiMock = mock<MercadoLibreRestAPI>()
    private var callMock = mock<Call<CurrencyRatio>>()
    private var mainActivityMock = mock<MainActivity>()


    @Before
    fun setup() {
        testSub = TestSubscriber<Float>()
        apiMock = mock<MercadoLibreRestAPI>()
        callMock = mock<Call<CurrencyRatio>>()
        mainActivityMock = mock<MainActivity>()
        Mockito.`when`(
            apiMock.getCurrencyRatio(
                ArgumentMatchers.anyString(),
                ArgumentMatchers.anyString()
            )
        ).thenReturn(callMock)


    }

    @Test
    fun testSuccess_currencyManager() {

        val currencyRatio = CurrencyRatio(62.0f)

        val response = Response.success(currencyRatio)
        Mockito.`when`(callMock.execute()).thenReturn(response)

        val currencyManager = CurrencyManager(apiMock)
        currencyManager.getRatio(Country.ARG.currency, Country.USA.currency).subscribe(testSub)

        testSub.assertNoErrors()
        testSub.assertValueCount(1)
        testSub.assertCompleted()
    }
}