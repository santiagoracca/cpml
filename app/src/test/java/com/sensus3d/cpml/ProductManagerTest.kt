package com.sensus3d.cpml

import com.sensus3d.cpml.API.MercadoLibreRestAPI
import com.sensus3d.cpml.API.ProductManager
import com.sensus3d.cpml.model.Country
import com.sensus3d.cpml.model.ProductBase
import com.sensus3d.cpml.model.SearchProductResponse
import com.sensus3d.cpml.model.Seller
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyInt
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import retrofit2.Call
import retrofit2.Response
import rx.observers.TestSubscriber

class ProductManagerTest {
    private var testSub = TestSubscriber<SearchProductResponse>()
    private var apiMock = mock<MercadoLibreRestAPI>()
    private var callMock = mock<Call<SearchProductResponse>>()
    var products =  ArrayList<ProductBase>(100)


    @Before
    fun setup() {
        testSub = TestSubscriber<SearchProductResponse>()
        apiMock = mock<MercadoLibreRestAPI>()
        callMock = mock<Call<SearchProductResponse>>()
        `when`(apiMock.searchForProducts(anyString(), anyString(), anyInt(), anyInt())).thenReturn(callMock)

        for(i in 0..49){
            products.add(ProductBase(
                "ML"+i.toString(),
                Seller(i),
                "Catan Manso Juego De Mesa ",
                i.toFloat(),
                "url",
                "url2",
                "Arg"
                )
            )
        }
    }

    @Test
    fun testSuccess_productManager() {

        val productResponse = SearchProductResponse((listOf()))

        val response = Response.success(productResponse)
        `when`(callMock.execute()).thenReturn(response)


        val newsManager = ProductManager(apiMock)
        newsManager.getProductResults(Country.ARG,"catan",10,100).subscribe(testSub)

        testSub.assertNoErrors()
        testSub.assertValueCount(1)
        testSub.assertCompleted()

    }


    @Test
    fun testSuccess_products() {
        // prepare
        val productSearchResponse = SearchProductResponse((products))
        val response = Response.success(productSearchResponse)
        `when`(callMock.execute()).thenReturn(response)

        // call
        val newsManager = ProductManager(apiMock)
        newsManager.getProductResults(Country.ARG, "catan", 0, 100).subscribe(testSub)
        // assert
        testSub.assertNoErrors()
        testSub.assertValueCount(1)
        testSub.assertCompleted()

        for (i in 0..49) {
            assert(testSub.onNextEvents[0].results[i].id == products[i].id)
            assert(testSub.onNextEvents[0].results[i].seller.id == i)
            assert(testSub.onNextEvents[0].results[i].title == products[i].title)
            assert(testSub.onNextEvents[0].results[i].price == products[i].price)
            assert(testSub.onNextEvents[0].results[i].thumbnail == products[i].thumbnail)
            assert(testSub.onNextEvents[0].results[i].permalink == products[i].permalink)
            assert(testSub.onNextEvents[0].results[i].currency_id == products[i].currency_id)
        }
    }
    @Test
    fun testSuccess_limit() {


    }
   /* @Test
    fun testError() {

        // prepare

        val responseError = Response.error<RedditNewsResponse>(500,

            ResponseBody.create(MediaType.parse("application/json"), ""))

        `when`(callMock.execute()).thenReturn(responseError)


        // call



        val newsManager = NewsManager(apiMock)

        newsManager.getNews("").subscribe(testSub)

        assert(testSub.onErrorEvents.size == 1)






    }
*/


}

inline fun <reified T : Any> mock(): T = Mockito.mock(T::class.java)