package com.sensus3d.cpml.model

/**
 * Model For Product Pictures
 */
data class ProductPicture(
    val id : String,
    val url : String
)