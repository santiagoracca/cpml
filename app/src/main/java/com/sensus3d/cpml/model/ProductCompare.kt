package com.sensus3d.cpml.model

import com.sensus3d.cpml.adapters.AdapterConstants
import kotlinx.android.parcel.Parcelize

/**
 * Model for comparing 2 products
 */
@Parcelize
class ProductCompare(val chosenProd: ProductBase, val comparingProd: ProductBase) : ProductBase(
    comparingProd.id,
    comparingProd.seller,
    comparingProd.title,
    comparingProd.price,
    comparingProd.thumbnail,
    comparingProd.permalink,
    comparingProd.currency_id)
{
    override fun getViewType() = AdapterConstants.PRODUCT_COMPARE


}