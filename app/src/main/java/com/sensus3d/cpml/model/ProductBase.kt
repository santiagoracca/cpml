package com.sensus3d.cpml.model

import android.os.Parcelable
import com.sensus3d.cpml.adapters.AdapterConstants
import com.sensus3d.cpml.adapters.ViewType
import kotlinx.android.parcel.Parcelize

/**
 * model for product
 */
@Parcelize
open class ProductBase(
    val id : String,
    val seller: Seller,
    val title: String,
    val price: Float,
    val thumbnail: String,
    val permalink : String,
    val currency_id: String

) : ViewType, Parcelable {


    override fun getViewType() = AdapterConstants.PRODUCT
    fun getPriceUSD() : Float?{
        val country = Country.fromCurrencyID(currency_id)
        country?.let {
            if (country.ratioUSD != 0.0f)
             return price * country.ratioUSD
        }
        return null
    }
}

