package com.sensus3d.cpml.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Model for obtaining a Product Response from API
 */
@Parcelize
class SearchProductResponse(val results: List<ProductBase>): Parcelable



