package com.sensus3d.cpml.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Model for MercadoLibreUser
 */
@Parcelize
data class MercadoLibreUser(
    val id: String,
    val nickname: String,
    val seller_reputation: SellerReputation
) : Parcelable

@Parcelize
data class SellerReputation(
    val level_id: String,
    val transactions: Transactions
) : Parcelable

@Parcelize
data class Transactions(
    val cancelled: Int,
    val completed: Int,
    val ratings: Ratings

) : Parcelable

@Parcelize
data class Ratings(val positive: Float) : Parcelable