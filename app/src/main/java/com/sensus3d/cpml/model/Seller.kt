package com.sensus3d.cpml.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Model for seller
 */
@Parcelize
data class Seller(
    val id: Int
) : Parcelable
