package com.sensus3d.cpml.model

/**
 * Enum of Countries in app
 */
enum class Country(val code : String, val currency : String, var ratioUSD: Float = 0.0f) {
    ARG("MLA", "ARS"),
    CHILE("MLC", "CLP"),
    BRAZIL("MLB", "BRL"),
    URUGUAY("MLU", "UYU"),
    MEXICO("MLM", "MXN"),
    USA("", "USD");

    companion object {
        private val map = values().associateBy(Country::currency)
        fun fromCurrencyID(currency: String) = map[currency]
    }
}

/*    {
        "id": "MCO",
        "name": "Colombia"
    },
    {
        "id": "MCR",
        "name": "Costa Rica"
    },
    {
        "id": "MPE",
        "name": "Perú"
    },
    {
        "id": "MHN",
        "name": "Honduras"
    },
    {
        "id": "MPY",
        "name": "Paraguay"
    },
    {
        "id": "MNI",
        "name": "Nicaragua"
    },
    {
        "id": "MLV",
        "name": "Venezuela"
    },
    {
        "id": "MBO",
        "name": "Bolivia"
    },
    {
        "id": "MCU",
        "name": "Cuba"
    },
    {
        "id": "MLM",
        "name": "Mexico"
    },
    {
        "id": "MEC",
        "name": "Ecuador"
    },
    {
        "id": "MPT",
        "name": "Portugal"
    },
    {
        "id": "MGT",
        "name": "Guatemala"
    },
    {
        "id": "MPA",
        "name": "Panamá"
    },
    {
        "id": "MSV",
        "name": "El Salvador"
    },
    {
        "id": "MLA",
        "name": "Argentina"
    },
    {
        "id": "MRD",
        "name": "Dominicana"
    },
    {
        "id": "MLB",
        "name": "Brasil"
    },
    {
        "id": "MLU",
        "name": "Uruguay"
    },
    {
        "id": "MLC",
        "name": "Chile"
    }
*/

