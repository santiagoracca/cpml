package com.sensus3d.cpml.model

/**
 * Model for product details, Currently only gets pictures
 */
data class ProductDetail(
    val pictures: List<ProductPicture>
)
