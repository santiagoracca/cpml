package com.sensus3d.cpml

/**
 * Preferences for Program
 */
object CPMLPreferences {
    /**
     * Amount of products requested on each call to API
     */
    const val PROD_LIMIT = 20
    var LAST_SEARCH : String = ""
}