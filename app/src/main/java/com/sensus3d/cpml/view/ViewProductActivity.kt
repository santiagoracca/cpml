package com.sensus3d.cpml.view

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.Snackbar
import com.sensus3d.cpml.API.ProductManager
import com.sensus3d.cpml.R
import com.sensus3d.cpml.decimalString
import com.sensus3d.cpml.loadImg
import com.sensus3d.cpml.model.MercadoLibreUser
import com.sensus3d.cpml.model.ProductBase
import com.sensus3d.cpml.model.Seller
import kotlinx.android.synthetic.main.product_fragment.*
import kotlinx.android.synthetic.main.view_product.*
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import android.graphics.drawable.BitmapDrawable


/**
 * View Product Detail Activity
 */
class ViewProductActivity : RxBaseActivity() {

    private val productManager by lazy { ProductManager() }
    private var currentMercadoLibreUser : MercadoLibreUser? = null
    private val MLUSER = "currentMLUser"
    private val PROD_IMG = "prodImg"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.view_product)

        val prodBase = intent.getParcelableExtra<ProductBase>("product")

        if (savedInstanceState != null && savedInstanceState.containsKey(PROD_IMG))
               productImage.setImageBitmap(savedInstanceState.getParcelable(PROD_IMG))
        else getProdDetails(prodBase.id)

        if(savedInstanceState != null && savedInstanceState.containsKey(MLUSER))
              setSellerUI(savedInstanceState.getParcelable(MLUSER)!!)
        else getSellerDetails(prodBase.seller)

        setProdBase(prodBase)


    }


    private fun setProdBase(prodBase: ProductBase) {
        productTitle.text = prodBase.title
        currencyChosenText.text = prodBase.currency_id
        priceView.text = prodBase.price.decimalString()

        val priceUSD = prodBase.getPriceUSD()
        priceUSD?.let { priceViewUSD.text = it.decimalString()}

        //linkML.text = prodBase.permalink
        mLButtonView.setOnClickListener {
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(prodBase.permalink)
            startActivity(i)
        }
    }
    private fun setSellerUI(mercadoLibreUser: MercadoLibreUser) {
        currentMercadoLibreUser = mercadoLibreUser
        sellerTitle.text = mercadoLibreUser.nickname
        sellerLevel.text = mercadoLibreUser.seller_reputation.level_id
        sellerTransactions.text = mercadoLibreUser.seller_reputation.transactions.completed.toString()
        sellerRatings.text = mercadoLibreUser.seller_reputation.transactions.ratings.positive.toString()
    }

    private fun getSellerDetails(seller: Seller) {
        val subscription = productManager.getUserDetails(seller.id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { mercadoLibreUser ->
                    setSellerUI(mercadoLibreUser)
                },
                { error ->
                    Snackbar.make(layoutVIew, "Cannot Retrieve Seller Info", Snackbar.LENGTH_LONG).show()
                }
            )
        subscriptions.add(subscription)
    }

    private fun getProdDetails(prodId: String) {
        val subscription = productManager.getProductDetails(prodId)
            .subscribeOn(Schedulers.io()) // thread API Request
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { details ->
                    productImage.loadImg(details.pictures[0].url)
                },
                { error ->
                    Snackbar.make(productRV, error.message ?: " currency Error ", Snackbar.LENGTH_LONG).show()
                }
            )
        subscriptions.add(subscription)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.clear()
        currentMercadoLibreUser?.let {
            outState.putParcelable(MLUSER, currentMercadoLibreUser)
        }
        if(productImage.drawable != null)
            outState.putParcelable(PROD_IMG,(productImage.drawable as BitmapDrawable).bitmap)
    }
}
