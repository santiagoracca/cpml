package com.sensus3d.cpml.view

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log

/**
 * Scroll Listener to add new products
 */
class InfiniteScrollListener(
    val func: () -> Unit,
    val layoutManager: LinearLayoutManager
) : RecyclerView.OnScrollListener() {

    private var previousTotal = 0
    private var loading = true
    private var visibleThreshold = 2
    private var firstVisibleItem = 0
    private var visibleItemCount = 0
    private var totalItemCount = 0

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)
        if (dy > 0) {
            visibleItemCount = recyclerView.childCount
            totalItemCount = layoutManager.itemCount
            firstVisibleItem = layoutManager.findFirstVisibleItemPosition()

            when {
                loading -> when {

                    (totalItemCount >= previousTotal) -> {
                        loading = false
                        previousTotal = totalItemCount
                    }

                    (totalItemCount < previousTotal - 1) -> previousTotal = 0

                }
                (!loading && (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)
                        ) -> {
                    Log.i("InfiniteScrollListener", "End reached");
                    func()
                    loading = true
                }
            }
        }
    }
}