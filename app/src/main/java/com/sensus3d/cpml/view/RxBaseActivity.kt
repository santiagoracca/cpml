package com.sensus3d.cpml.view

import android.support.v7.app.AppCompatActivity
import rx.subscriptions.CompositeSubscription

/**
 * Base Activity to manage loading and unloading subscriptions of RxJava
 */
open class RxBaseActivity: AppCompatActivity() {
    protected var subscriptions = CompositeSubscription()

    override fun onResume() {
        super.onResume()
        subscriptions = CompositeSubscription()
    }

    override fun onPause() {
        super.onPause()
        if (!subscriptions.isUnsubscribed) {
            subscriptions.unsubscribe()
        }
        subscriptions.clear()
    }
}