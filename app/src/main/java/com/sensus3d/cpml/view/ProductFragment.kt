package com.sensus3d.cpml.view

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sensus3d.cpml.*
import com.sensus3d.cpml.adapters.ProductAdapter
import com.sensus3d.cpml.model.SearchProductResponse
import kotlinx.android.synthetic.main.product_fragment.*

/**
 * Fragment that contains Recycler view used to Display Products, Used by BaseProductListActivity
 */
class ProductFragment : Fragment() {


    private  val PRODUCTS_SAVED  = "savedProds"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return container?.inflate(R.layout.product_fragment)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val linearLayoutManager = LinearLayoutManager(context)

        productRV.apply {
            setHasFixedSize(true)
            layoutManager = linearLayoutManager
            clearOnScrollListeners()
            val prodActivity = activity as BaseProductListActivity
            addOnScrollListener(
                InfiniteScrollListener(
                    prodActivity.getRefreshFunc(),
                    linearLayoutManager
                )
            )
            addItemDecoration(DividerItemDecoration(context, linearLayoutManager.orientation))
            initAdapter()
            if (savedInstanceState != null && savedInstanceState.containsKey(PRODUCTS_SAVED)) {
                val savedProducts = savedInstanceState.get(PRODUCTS_SAVED) as SearchProductResponse
                val productAdapter = adapter as ProductAdapter
                productAdapter.cleanItems()
                productAdapter.addLoading()
                productAdapter.addProducts(savedProducts.results)
            }
        }

    }

    /*override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        val prodActivity = activity as BaseProductListActivity

        if (savedInstanceState != null && savedInstanceState.containsKey(prodActivity.getInstanceStateName())) {
            val savedProducts = savedInstanceState.get(prodActivity.getInstanceStateName()) as SearchProductResponse
            val productAdapter = productRV.adapter as ProductAdapter
            productAdapter.cleanItems()
            productAdapter.addLoading()
            productAdapter.addProducts(savedProducts.results)
        }
    }*/
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        val products = (productRV.adapter as ProductAdapter).getProducts()
        if (products.results.isNotEmpty())
            outState.putParcelable(PRODUCTS_SAVED, products)
    }


    private fun initAdapter() {
        if (productRV.adapter == null) {
            productRV.adapter = ProductAdapter()
        }
    }
}



