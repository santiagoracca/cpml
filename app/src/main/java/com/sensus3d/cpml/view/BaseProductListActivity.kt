package com.sensus3d.cpml.view

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import com.sensus3d.cpml.model.Country
import com.sensus3d.cpml.API.CurrencyManager
import com.sensus3d.cpml.API.ProductManager
import com.sensus3d.cpml.R
import com.sensus3d.cpml.adapters.ProductAdapter
import kotlinx.android.synthetic.main.product_fragment.*

/**
 * Base Activity for Activities that display products in a Fragment
 */
abstract class BaseProductListActivity : RxBaseActivity() {

    protected val productFragment by lazy { ProductFragment() }
    protected var countryChosen: Country = Country.ARG
    protected val currencyManager by lazy { CurrencyManager() }
    protected val productManager by lazy { ProductManager() }
    protected var refreshCount = 0

    private  val PRODUCTS_SAVED  = "savedProds"



    fun changeFragment(idContainer : Int,f: Fragment, cleanStack: Boolean = false) {
        val ft = supportFragmentManager.beginTransaction()

        if (cleanStack)
            clearBackStack()

        ft.setCustomAnimations(
            R.anim.abc_fade_in,
            R.anim.abc_fade_out,
            R.anim.abc_popup_enter,
            R.anim.abc_popup_exit
        )

        ft.replace(idContainer, f)
        ft.addToBackStack(null)
        ft.commit()
    }


    fun clearBackStack() {
        val manager = supportFragmentManager

        if (manager.backStackEntryCount > 0) {
            val first = manager.getBackStackEntryAt(0)
            manager.popBackStack(first.id, FragmentManager.POP_BACK_STACK_INCLUSIVE)
        }
    }


    /**
     * Finish activity when reaching the last fragment.
     */

    override fun onBackPressed() {

        val fragmentManager = supportFragmentManager
        if (fragmentManager.backStackEntryCount > 1) {
            fragmentManager.popBackStack()
        } else {

            finish()

        }

    }

    /**
     * get Func to be called be ScrollListener on Refresh
     */
    abstract fun getRefreshFunc(): () -> Unit


   /* fun saveInstanceStateFromFragment(outState: Bundle) {
        val products = (productRV.adapter as ProductAdapter).getProducts()
        if (products.results.isNotEmpty())
            outState.putParcelable(PRODUCTS_SAVED, products)
    }
*/
   // fun getInstanceStateName(): String = PRODUCTS_SAVED
}