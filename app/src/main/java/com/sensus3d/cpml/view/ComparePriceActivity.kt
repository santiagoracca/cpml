package com.sensus3d.cpml.view

import android.content.Intent
import android.os.Bundle
import com.sensus3d.cpml.adapters.ProductAdapter
import com.sensus3d.cpml.model.ProductBase
import kotlinx.android.synthetic.main.compare_price.*
import kotlinx.android.synthetic.main.product_fragment.*

import android.net.Uri
import android.support.design.widget.Snackbar
import com.sensus3d.cpml.*
import com.sensus3d.cpml.CPMLPreferences.LAST_SEARCH
import com.sensus3d.cpml.model.Country
import com.sensus3d.cpml.model.ProductCompare
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

/**
 * Compare Prices activity
 */
class ComparePriceActivity : BaseProductListActivity() {


    private val COMP_PRODUCTS = "compProducts"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.compare_price)
        countryChosen = Country.USA
        val chosenProd = intent.getParcelableExtra<ProductBase>("product")
        setChosenProd(chosenProd)

          if (savedInstanceState == null) {
              changeFragment(R.id.fragmentContainerCompare, productFragment)
          }

        addListenerForCompareCountryIcons(chosenProd)


          frameChosen.setOnClickListener {
              val intent = Intent(it.context, ViewProductActivity::class.java)
              intent.putExtra("product", chosenProd)
              it.context.startActivity(intent)
          }
    }

    private fun addListenerForCompareCountryIcons(chosenProd: ProductBase) {
        argIconCompare.setOnClickListener { toggleCountryAndCompare(Country.ARG, chosenProd) }
        chileIconCompare.setOnClickListener { toggleCountryAndCompare(Country.CHILE, chosenProd) }
        brazilIconCompare.setOnClickListener { toggleCountryAndCompare(Country.BRAZIL, chosenProd) }
        uruguayIconCompare.setOnClickListener { toggleCountryAndCompare(Country.URUGUAY, chosenProd) }
        mexicoIconCompare.setOnClickListener { toggleCountryAndCompare(Country.MEXICO, chosenProd) }
    }


    private fun toggleCountryAndCompare(selectedCountry: Country, chosenProd: ProductBase) {
        if (countryChosen != selectedCountry) {
            countryChosen = selectedCountry

            compareProducts(chosenProd, selectedCountry)
        }
    }

    private fun setChosenProd(chosenProd: ProductBase) {
        productDescriptionChosen.text = chosenProd.title
        productImageChosen.loadImg(chosenProd.thumbnail)
        productPriceChosen.text = chosenProd.price.decimalString()
        currencyTextChosen.text = chosenProd.currency_id
        val country = Country.fromCurrencyID(chosenProd.currency_id)
        country?.let {
            if (country.ratioUSD != 0.0f)
                productPriceUSDChosen.text = (chosenProd.price * country.ratioUSD).decimalString()
        }
        mLButton.setOnClickListener {
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(chosenProd.permalink)
            startActivity(i)
        }
    }

    private fun compareProducts(chosenProd: ProductBase, country: Country) {
        if (country == Country.USA) return
        val productAdapter = productRV.adapter as ProductAdapter
        productAdapter.cleanItems()
        productAdapter.addLoading()
        refreshCount = 0
        getCompareProducts(chosenProd)
    }

    private fun getCompareProducts(chosenProd: ProductBase, search : String = LAST_SEARCH ,offset: Int = 0, limit: Int = CPMLPreferences.PROD_LIMIT) {
        val subscription = productManager.getProductResults(countryChosen,search,offset)
            .subscribeOn(Schedulers.io()) // thread API Request
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { productData ->
                    if (productData.results.isNotEmpty()) {
                        val comparableProds = productData.results.map {
                            ProductCompare(chosenProd, it)
                        }
                        (productRV.adapter as ProductAdapter).addProducts(comparableProds)
                        refreshCount++
                    } else {
                        (productRV.adapter as ProductAdapter).removeLastLoading()
                        Snackbar.make(productRV, " No More Results ", Snackbar.LENGTH_LONG).show()
                    }
                },
                { error ->
                    Snackbar.make(productRV, error.message ?: " error ", Snackbar.LENGTH_LONG).show()
                }
            )
        subscriptions.add(subscription)
    }

    override fun getRefreshFunc(): () -> Unit {
        val chosenProd = intent.getParcelableExtra<ProductBase>("product")
        return { getCompareProducts(chosenProd, LAST_SEARCH,refreshCount * CPMLPreferences.PROD_LIMIT)}
    }
}