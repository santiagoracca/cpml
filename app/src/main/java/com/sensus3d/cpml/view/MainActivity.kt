package com.sensus3d.cpml.view

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.view.inputmethod.EditorInfo
import com.sensus3d.cpml.*
import com.sensus3d.cpml.CPMLPreferences.LAST_SEARCH
import com.sensus3d.cpml.adapters.ProductAdapter
import com.sensus3d.cpml.model.Country

import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.product_fragment.*
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

/**
 * Activity used to search and Display Products
 */
class MainActivity : BaseProductListActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        getCurrencyRatios()


        if (savedInstanceState == null) {
            changeFragment(R.id.fragmentContainer, ProductFragment())
        }

        addListenersToCountryIcons()

        searchBar.setOnEditorActionListener { textView, actionId, keyEvent ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {

                searchProducts(textView.text.toString())
                return@setOnEditorActionListener true
            }
            false
        }

    }

    private fun addListenersToCountryIcons() {
        argIcon.setOnClickListener { toggleCountryAndSearch(Country.ARG) }
        chileIcon.setOnClickListener { toggleCountryAndSearch(Country.CHILE) }
        brazilIcon.setOnClickListener { toggleCountryAndSearch(Country.BRAZIL) }
        uruguayIcon.setOnClickListener { toggleCountryAndSearch(Country.URUGUAY) }
        mexicoIcon.setOnClickListener { toggleCountryAndSearch(Country.MEXICO) }
    }


    private fun toggleCountryAndSearch(countrySelected: Country) {
        if (countryChosen != countrySelected) {
            countryChosen = countrySelected
            searchProducts(searchBar.text.toString())
        }
    }

    private fun searchProducts(search: String) {
        if (search.isEmpty()) return
        val productAdapter = productRV.adapter as ProductAdapter
        refreshCount = 0
        productAdapter.cleanItems()
        productAdapter.addLoading()
        checkForConversionRatio()
        getProductResults(search)
    }



    private fun getProductResults(search: String, offset: Int = 0, limit: Int = CPMLPreferences.PROD_LIMIT) {
        val subscription = productManager.getProductResults(countryChosen, search, offset, limit)
            .subscribeOn(Schedulers.io()) // thread API Request
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { productData ->
                    if (productData.results.isNotEmpty()) {
                        (productRV.adapter as ProductAdapter).addProducts(productData.results)
                        refreshCount++
                    } else {
                        (productRV.adapter as ProductAdapter).removeLastLoading()
                        Snackbar.make(productRV, " No More Results ", Snackbar.LENGTH_LONG).show()
                    }
                },
                { error ->
                    Snackbar.make(productRV, error.message ?: " error ", Snackbar.LENGTH_LONG).show()
                }
            )
        subscriptions.add(subscription)
    }

    override fun getRefreshFunc(): () -> Unit {
        return { getProductResults(LAST_SEARCH, refreshCount * CPMLPreferences.PROD_LIMIT) }
    }

    private fun checkForConversionRatio() {
        if(countryChosen.ratioUSD == 0.0f)
            getRatioForCountry(countryChosen)
    }

    fun getCurrencyRatios() {
        for (country in Country.values()) {
            getRatioForCountry(country)
        }
    }

    private fun getRatioForCountry(country: Country) {
        val subscription = currencyManager.getRatio(country.currency, Country.USA.currency)
            .subscribeOn(Schedulers.io()) // thread API Request
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { ratio ->
                    country.ratioUSD = ratio
                },
                { error ->
                    Snackbar.make(
                        productRV,
                        "Could not fetch Currency conversion Rates",
                        Snackbar.LENGTH_LONG
                    ).show()
                }
            )
        subscriptions.add(subscription)
    }
}



