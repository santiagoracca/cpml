package com.sensus3d.cpml.adapters

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.sensus3d.cpml.*
import com.sensus3d.cpml.model.ProductCompare
import com.sensus3d.cpml.view.ViewProductActivity
import kotlinx.android.synthetic.main.product_item_compare.view.*

/**
 * Delegate adapter used to compare Products
 */
class ProductCompareDelegateAdapter : ViewTypeDelegateAdapter {

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder =
        ProductCompareViewHolder(parent)

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: ViewType) {
        holder as ProductCompareViewHolder
        val product = item as ProductCompare
        holder.bind(product)
        holder.itemView.setOnClickListener {
            val intent = Intent(it.context, ViewProductActivity::class.java)
            intent.putExtra("product", product)
            it.context.startActivity(intent)
        }
    }

    class ProductCompareViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
        parent.inflate(R.layout.product_item_compare)
    ) {

        fun bind(compareProd: ProductCompare) = with(itemView) {
            productImageComp.loadImg(compareProd.thumbnail)
            productDescriptionComp.text = compareProd.title
            productPriceComp.text = compareProd.price.decimalString()
            currencyTextComp.text = compareProd.currency_id
            val priceUSDCompare = compareProd.getPriceUSD()
            priceUSDCompare?.let {

                productPriceUSDComp.text = priceUSDCompare.decimalString()

                val priceUSDChosen = compareProd.chosenProd.getPriceUSD()
                priceUSDChosen?.let {
                    comparePriceText.text =
                        (priceUSDCompare - priceUSDChosen).decimalString()
                }
            }

        }
    }


}
