package com.sensus3d.cpml.adapters

/**
 * Adapter Constants for delegate Adapters
 */
object AdapterConstants {
    const val PRODUCT = 1
    const val LOADING = 2
    const val PRODUCT_COMPARE = 3
}