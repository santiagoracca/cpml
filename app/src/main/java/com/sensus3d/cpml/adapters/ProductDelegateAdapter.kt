package com.sensus3d.cpml.adapters

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.sensus3d.cpml.*
import com.sensus3d.cpml.model.ProductBase
import com.sensus3d.cpml.view.ComparePriceActivity
import com.sensus3d.cpml.view.ViewProductActivity
import kotlinx.android.synthetic.main.product_item.view.*

/**
 * Delegate adapter used to List Products
 */
class ProductDelegateAdapter : ViewTypeDelegateAdapter {

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder =
        ProductViewHolder(parent)

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: ViewType) {
        holder as ProductViewHolder
        val product = item as ProductBase
        holder.bind(product)
        holder.itemView.setOnClickListener {
            val intent = Intent(it.context, ViewProductActivity::class.java)
            intent.putExtra("product", product)
            it.context.startActivity(intent)
        }


    }

    class ProductViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
        parent.inflate(R.layout.product_item)
    ) {

        fun bind(item: ProductBase) = with(itemView) {
            productImage.loadImg(item.thumbnail)
            productDescription.text = item.title
            productPrice.text = item.price.decimalString()
            currencyText.text = item.currency_id
            val priceUSD = item.getPriceUSD()
            priceUSD?.let {
                productPriceUSD.text = priceUSD.decimalString()
            }
            compareButton.setOnClickListener{
                val intent = Intent(it.context, ComparePriceActivity::class.java)
                intent.putExtra("product", item)
                it.context.startActivity(intent)
            }
        }
    }
}