package com.sensus3d.cpml.adapters

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.sensus3d.cpml.R
import com.sensus3d.cpml.inflate

/**
 * Delegate Adapter for loading
 */
class LoadingDelegateAdapter : ViewTypeDelegateAdapter {

    override fun onCreateViewHolder(parent: ViewGroup) =
        LoadingViewHolder(parent)

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: ViewType) {
    }

    /***
     * Because RecyclerView.ViewHolder is abstract
     */
    class LoadingViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(parent.inflate(R.layout.product_loading))

}