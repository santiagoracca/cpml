package com.sensus3d.cpml.adapters

import android.support.v4.util.SparseArrayCompat
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.sensus3d.cpml.model.ProductBase
import com.sensus3d.cpml.model.SearchProductResponse

/**
 * Adapter for RecyclerView that displays products ot CompareProducts depending on the view
 */
class ProductAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    /**
     * Items to render in Recycler view
     */
    private var itemsToRender: ArrayList<ViewType>
    /**
     * Map for the different kinds of adapters used by onCreateViewHolder() and onBindViewHolder() to display different kinds of items
     */
    private var delegateAdapters = SparseArrayCompat<ViewTypeDelegateAdapter>()

    /**
     * added Loading Object here to manage ViewType, so I dont have to create a new class
     */
    private val loadingItem = object : ViewType {
        override fun getViewType() = AdapterConstants.LOADING
    }

    init {
        delegateAdapters.put(AdapterConstants.LOADING, LoadingDelegateAdapter())
        delegateAdapters.put(AdapterConstants.PRODUCT, ProductDelegateAdapter())
        delegateAdapters.put(AdapterConstants.PRODUCT_COMPARE, ProductCompareDelegateAdapter())
        itemsToRender = ArrayList()

        //itemsToRender.add(ProductBase("Patrick","Oculus",1500,"http://mla-s1-p.mlstatic.com/736822-MLA31134401237_062019-I.jpg","https://articulo.mercadolibre.com.ar/MLA-787131438-oculus-rift-s-all-in-one-vr-cabezal-y-mandos-nuevo-2019-_JM"))
        //itemsToRender.add(ProductBase("Patrick","Oculus 2",2000,"http://mla-s2-p.mlstatic.com/696246-MLA31099004630_062019-I.jpg","https://articulo.mercadolibre.com.ar/MLA-787131438-oculus-rift-s-all-in-one-vr-cabezal-y-mandos-nuevo-2019-_JM"))
    }

    /**
     * Get Products currently listed
     */
    fun getProducts(): SearchProductResponse {
        return SearchProductResponse(
        itemsToRender.filter {
            (it.getViewType() != AdapterConstants.LOADING)
        }.map {
            it as ProductBase
        })
    }

    /**
     * Add Loading item
     */
    fun addLoading() {
        itemsToRender.add(loadingItem)
        notifyItemInserted(itemsToRender.size)
    }

    /**
     * Add products to list
     */
    fun addProducts(prod: List<ProductBase>) {
        val initPosition = itemsToRender.size - 1
        itemsToRender.removeAt(initPosition)
        notifyItemRemoved(initPosition)

        itemsToRender.addAll(prod)
        itemsToRender.add(loadingItem)
        notifyItemRangeChanged(initPosition, itemsToRender.size + 1)
    }

    /**
     * Removes loading from las slot
     */
    fun removeLastLoading() {
        val initPosition = itemsToRender.size - 1

        if (itemsToRender[initPosition].getViewType() == AdapterConstants.LOADING) {
            itemsToRender.removeAt(initPosition)
            notifyItemRemoved(initPosition)
        }
    }

    /**
     * Clears all the items in the list
     */
    fun cleanItems() {
        if (itemsToRender.isEmpty()) return

        notifyItemRangeRemoved(0, itemsToRender.size - 1)
        itemsToRender.clear()
    }

    override fun getItemCount(): Int {
        return itemsToRender.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return delegateAdapters.get(viewType, LoadingDelegateAdapter()).onCreateViewHolder(parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        delegateAdapters.get(getItemViewType(position))?.onBindViewHolder(holder, this.itemsToRender[position])
    }

    override fun getItemViewType(position: Int): Int {
        return this.itemsToRender[position].getViewType()
    }


}
