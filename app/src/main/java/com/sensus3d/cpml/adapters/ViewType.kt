package com.sensus3d.cpml.adapters


interface ViewType {
    fun getViewType(): Int
}