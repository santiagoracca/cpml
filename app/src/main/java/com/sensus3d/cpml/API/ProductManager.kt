package com.sensus3d.cpml.API

import com.sensus3d.cpml.CPMLPreferences
import com.sensus3d.cpml.CPMLPreferences.LAST_SEARCH
import com.sensus3d.cpml.model.Country
import com.sensus3d.cpml.model.MercadoLibreUser
import com.sensus3d.cpml.model.ProductDetail
import com.sensus3d.cpml.model.SearchProductResponse
import rx.Observable

/**
 * * Manager that handles the calls for products( and users)
 */
class ProductManager(private val mercadoLibreAPI: MercadoLibreRestAPI = MercadoLibreRestAPI()) {


    fun getProductResults(country: Country, searchProd: String = LAST_SEARCH, offset: Int = 0, limit : Int = CPMLPreferences.PROD_LIMIT): Observable<SearchProductResponse> {
        LAST_SEARCH = searchProd
        return Observable.create { subscriber ->
            val callResponse = mercadoLibreAPI.searchForProducts(country.code, searchProd,offset, limit)

            val response = callResponse.execute()

            if (response.isSuccessful) {
                subscriber.onNext(response.body())
                subscriber.onCompleted()
            } else {
                subscriber.onError(Throwable(response.message()))
            }
        }
    }

    fun getProductDetails(prodId: String): Observable<ProductDetail> {
        return Observable.create { subscriber ->
            val callResponse = mercadoLibreAPI.getProdDetails(prodId)

            val response = callResponse.execute()

            if (response.isSuccessful) {
                subscriber.onNext(response.body())
                subscriber.onCompleted()
            } else {
                subscriber.onError(Throwable(response.message()))
            }
        }
    }
    //cambiar a UserManager,
    fun getUserDetails(userId: Int): Observable<MercadoLibreUser> {
        return Observable.create { subscriber ->
            val callResponse = mercadoLibreAPI.getUserById(userId)

            val response = callResponse.execute()

            if (response.isSuccessful) {
                subscriber.onNext(response.body())
                subscriber.onCompleted()
            } else {
                subscriber.onError(Throwable(response.message()))
            }
        }
    }

}