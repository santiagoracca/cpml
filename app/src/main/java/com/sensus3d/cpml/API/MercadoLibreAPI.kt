package com.sensus3d.cpml.API

import com.sensus3d.cpml.model.CurrencyRatio
import com.sensus3d.cpml.model.MercadoLibreUser
import com.sensus3d.cpml.model.ProductDetail
import com.sensus3d.cpml.model.SearchProductResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Mercado Libre API interface
 */
interface  MercadoLibreAPI {

    @GET("/sites/{country}/search")
    fun searchForProducts(@Path("country") country : String,
                          @Query("q") search: String,
                          @Query("offset") offset: String,
                          @Query("limit") limit: String)
            : Call<SearchProductResponse>


    @GET("/currency_conversions/search")
    fun getCurrencyRatio(@Query("from") currency1 : String,
                          @Query("to") currency2 : String)
            :Call<CurrencyRatio>

    @GET("/items/{prodId}")
    fun getProdDetails(@Path("prodId") prodId : String)
            :Call<ProductDetail>

    @GET("/users/{userId}")
    fun gerUserById(@Path("userId") userId : Int)
            :Call<MercadoLibreUser>

}