package com.sensus3d.cpml.API

import com.sensus3d.cpml.model.CurrencyRatio
import com.sensus3d.cpml.model.MercadoLibreUser
import com.sensus3d.cpml.model.ProductDetail
import com.sensus3d.cpml.model.SearchProductResponse
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

/**
 * Mercado Libre API impl
 */
class MercadoLibreRestAPI {

    private val mLApi: MercadoLibreAPI

    init {
        val retrofit = Retrofit.Builder()
            .baseUrl("https://api.mercadolibre.com")
            .addConverterFactory(MoshiConverterFactory.create())
            .build()
        mLApi = retrofit.create(MercadoLibreAPI::class.java)
    }

    fun searchForProducts(country : String,search: String,offset : Int ,limit: Int ): Call<SearchProductResponse> {
        return mLApi.searchForProducts(country, search ,offset.toString(),limit.toString())
    }

    fun getCurrencyRatio(currency1 : String,currency2: String): Call<CurrencyRatio> {
        return mLApi.getCurrencyRatio(currency1,currency2)
    }

    fun getProdDetails(prodId: String) : Call<ProductDetail> {
        return mLApi.getProdDetails(prodId)
    }

    fun getUserById(userId: Int) : Call<MercadoLibreUser> {
        return mLApi.gerUserById(userId)
    }

}