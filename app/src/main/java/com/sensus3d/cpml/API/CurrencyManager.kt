package com.sensus3d.cpml.API

import rx.Observable

/**
 * Manager that handles the calls for currency Ratios of the different countries
 */
class CurrencyManager(private val mercadoLibreAPI: MercadoLibreRestAPI = MercadoLibreRestAPI()){


    fun getRatio(currency1: String, currency2: String): Observable<Float> {
        return Observable.create { subscriber ->
            val callResponse = mercadoLibreAPI.getCurrencyRatio(currency1, currency2)

            val response = callResponse.execute()

            if (response.isSuccessful) {
                subscriber.onNext(response.body().ratio)
                subscriber.onCompleted()
            } else {
                subscriber.onError(Throwable(currency1))
            }
        }
    }

}