# CPML - Comprare Prices Mercado Libre
> Si lo se no el nombre mas original..

## Table of contents
* [General info](#general-info)
* [Technologies/Languages](#technologies)
* [Setup](#setup)
* [Features](#features)
* [Status](#status)
* [To-do](#To-do)
* [Contact](#contact)

## General info
Compare Prices in Mercado Libre

## Technologies/Language
* Kotlin - Wanted to learn it, Cooler than Java
* RetroFit - API interface library
* Moshi - Parsing Json/ works best with Retrofit
* RxJava/Android - CallBacks and Observers, Threading
* Picasso - Used to load images from URL

## Setup
Clone and run

## Features
*Compares Prices in 5 Countries using up-to-date conversion Rates(important in Argentina, it changes everyday)(veryeasy to add new ones)
*View Products


## To-do:
* Localize
* Offset Bug in Argentina, Brazil (offset does not work in the API)
* Change Ugliest UI in the world (specially the green background when comparing, oh and the view Product XD)
* More Testing (Instrumented and Unit)
* Better search algorithm with keywords, price analitycs, categories
* Toggle switch for countries so I know which one is selected
* Show all pictures in ViewProduct Activity
* Better Documentation 
* Circle CI implementation


## Status
Project is: _in progress_

## Contact
Created by Santiago Racca - feel free to contact me at stgoRacca@gmail.com